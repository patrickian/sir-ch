from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup as soup

from automobile.model.car import CarAd

home_page = 'https://awesome-table.com/-KrkkVbKZR_HZ2x8lsHk/view'

def search(search_parameter, max_pages):
    results = []
    try:
        driver = webdriver.Chrome()
        driver.implicitly_wait(15)

        driver.get(home_page)
        driver.find_element_by_xpath('//div[contains(@id,"searchFilter")]/div/input').send_keys(search_parameter)

        while max_pages > 0:
            results.extend(extract_content(driver.page_source))
            next_page = driver.find_element_by_xpath('//*[@id="paginationBottom"]/div/div[2]/div/svg/g/path')
            driver.execute_script('arguments[0].click();', next_page)
            max_pages -= 1

    except Exception as ex:
        print(ex)
        print('End of page reached.')
    finally:
        driver.close()
    return results


def extract_content(html):
    page_soup = soup(html, 'html.parser')
    odd_classified_ads = page_soup.findAll('tr', {'class': 'google-visualization-table-tr-odd'})
    page_contents = []
    for classified_ad in classified_ads:
        try:
            print (classified_ad)
        except Exception as ex:
            print(ex)
            print('Inconsistent data.')
    
    return page_contents