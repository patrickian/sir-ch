from pc_components import pchub

search_parameter = input('What would you like to search for? ')
max_pages = int(input('Enter the maximum number of pages you would like: '))
print(f'Searching for {search_parameter}...')
results = []

results.extend(pchub.search(search_parameter, max_pages))

for result in results:
    print(f"{result.title} | {result.price} | {result.link}")
