from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup as soup

from automobile.model.car import CarAd


home_page = 'https://www.ebay.ph/'


def search(driver, search_parameter, max_pages):
    results = []
    try:
        driver.get(home_page)
        driver.find_element_by_xpath('//*[@id="gh-ac"]').send_keys(search_parameter)
        driver.find_element_by_xpath('//*[@id="gh-btn"]').click()

        try:
            while True:
                results.extend(extract_content(driver.page_source))
                max_pages -= 1
                if max_pages <= 0:
                    break
                next_page = driver.find_element_by_xpath('//*[@id="Pagination"]/tbody/tr/td[@class="pagn-next"]/a')
                driver.execute_script('arguments[0].click();', next_page)
        except Exception as ex:
            print(f'Ebay PH end of the page has been reached.')

    except Exception as ex:
        print('An error occured while searching in Ebay PH.')
    return results


def extract_content(html):
    page_soup = soup(html, 'html.parser')
    classified_ads = page_soup.findAll('li', {'class': 'sresult'})
    page_contents = []
    for classified_ad in classified_ads:
        try:
            header = classified_ad.find('h3', {'class': 'lvtitle'}).find('a')
            price_head = classified_ad.find('ul', {'class': 'lvprices'}).find('li', {'class': 'lvprice'})

            title = header.text.strip()
            link = f"{header['href']}"
            price = price_head.span.text.strip()

            page_contents.append(CarAd(title, price, link))
        except Exception:
            pass
    
    return page_contents
