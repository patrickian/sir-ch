from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as soup

from automobile.model.car import CarAd


home_page = 'https://www.carfinderph.com'


def search(driver, search_parameter, max_pages):
    results = []
    try:
        driver.get(home_page)

        search_field = driver.find_element_by_xpath('//*[@id="autocomplete"]')
        search_field.send_keys(search_parameter)
        search_field.send_keys(Keys.RETURN)

        all_words_button = driver.find_element_by_xpath('//*[@id="css_INPUT_7"]')
        driver.execute_script('arguments[0].click();', all_words_button)
        go_button = driver.find_element_by_xpath('//*[@id="area_keyword"]/form/div[@class="two-inline"]/div/input')
        driver.execute_script('arguments[0].click();', go_button)

        try:
            while True:
                results.extend(extract_content(driver.page_source))
                max_pages -= 1
                if max_pages <= 0:
                    break
                next_page_link = driver.find_element_by_xpath('//*[@id="area_keyword"]/div/ul/li[@class="navigator rs"]/a')
                driver.execute_script('arguments[0].click();', next_page_link)
        except Exception as ex:
            print(f'Carfinder end of the page has been reached.')
    except Exception:
        print('Something went wrong while searching in carfinder.')
    return results


def extract_content(html):
    page_soup = soup(html, 'html.parser')
    classified_ads = page_soup.findAll('article', {'class': 'item'})
    page_contents = []
    for classified_ad in classified_ads:
        try:
            main_column = classified_ad.find('div', {'class': 'main-column'})
            info = main_column.find('ul', {'class': 'ad-info'})
            title = info.find('li', {'class': 'title'}).find('a').text.strip()

            link_element = main_column.find('a')
            link = link_element['href'].strip()

            price = info.find('li', {'class': 'system'}).find('span', {'class': 'price-tag'}).find('span').text.strip()
            page_contents.append(CarAd(title, price, link))    
        except Exception:
            pass
    
    return page_contents
