class CarAd:
    def __init__(self, title, price, link, location=None):
        self.title = title
        self.price = self.numerify(price)
        self.link = link
        self.location = location

    def numerify(self, text):
         return ''.join([c for c in text if c in '1234567890.'])

