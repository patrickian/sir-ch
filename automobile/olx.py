from selenium import webdriver
from bs4 import BeautifulSoup as soup
from automobile.model.car import CarAd


home_page = 'https://www.olx.ph'


def search(driver, search_parameter, max_pages):
    car_ads = []
    try:
        # Go to OLX homepage.
        driver.get(home_page)

        # Find the search input element and send the search parameters.
        driver.find_element_by_xpath("//input[@id='searchKeyword']").send_keys(search_parameter)

        # Find the search button and click it.
        driver.find_element_by_xpath("//button[@id='btnSubmit']").click()

        try:
            while True:
                # get_data_from_page_use_selenium(driver)
                car_ads.extend(extract_content(driver.page_source))
                max_pages -= 1
                if max_pages <= 0:
                    break
                # get_data_from_page_use_lxml(driver.page_source)
                next_page_link = driver.find_element_by_xpath("//li[@class='pagination-next']/a")
                driver.execute_script('arguments[0].click();', next_page_link)
        except:
            print(f'OLX end of the page has been reached.')

    except Exception as ex:
        print('An error occured while searching OLX.')

    return car_ads


def extract_content(html):
    ads = []
    page_soup = soup(html, 'html.parser')
    classified_ads = page_soup.findAll('div', {'class': 'item'})

    for classified_ad in classified_ads:
        try:
            title = classified_ad.find('span', {'class': 'title'}).text.strip()
            location = classified_ad.find('span', {'class': 'location'}).text.strip()
            price = classified_ad.find('span', {'class': 'price'}).text.strip()
            link_element = classified_ad.find('a', {'itemprop': 'url'})

            link = f"{home_page}{link_element['href']}"
            ads.append(CarAd(title, price, link, location))
        except Exception:
            pass
    return ads


# NOT YET WORKING PROPERLY
def get_data_from_page_use_selenium(driver):
    try:
        # Get all the usable items in the page.
        classified_ads = driver.find_elements_by_xpath("//div[@class='item']")

        for classified_ad in classified_ads:
            title = classified_ad.find_element_by_xpath(".//span[@class='title']")
            location = classified_ad.find_element_by_xpath(".//div/span[@class='location']")
            # price = classified_ad.find_element_by_xpath(".//span[@class='price']")
            if title is not None and location is not None:
                print(f"{title.text.strip()} | {location.text.strip()}")
    except Exception as e:
        print(e.__str__())
