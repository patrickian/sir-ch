from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup as soup

from automobile.model.car import CarAd


home_page = 'https://philkotse.com'


def search(driver, search_parameter, max_pages):
    results = []
    try:
        driver.get(home_page)
        driver.find_element_by_xpath('//*[@id="KeywordSearch"]').send_keys(search_parameter)
        driver.find_element_by_xpath('//*[@id="search"]').click()

        try:
            while True:
                results.extend(extract_content(driver.page_source))
                max_pages -= 1
                if max_pages <= 0:
                    break
                next_page = driver.find_element_by_xpath('//div[@class="page"]/ul/li/a[@class="next-page"]')
                driver.execute_script('arguments[0].click();', next_page)
        except Exception as ex:
            print('Philkotse end of page has been reached.')

    except Exception as ex:
        print('An error occured while searching in philkotse.')
    return results


def extract_content(html):
    page_soup = soup(html, 'html.parser')
    classified_ads = page_soup.findAll('div', {'class': 'box-prd'})
    page_contents = []
    for classified_ad in classified_ads:
        try:
            info = classified_ad.find('div', {'class': 'content'})
            header = info.h2.a
            price_head = info.find('div', {'class': 'fweight-bold'})

            title = header.text.strip()
            link = f"{home_page}{header['href']}"
            price = price_head.text.strip()

            page_contents.append(CarAd(title, price, link))
        except Exception as ex:
            pass
    
    return page_contents
