from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup as soup

from automobile.model.car import CarAd


home_page = 'https://www.carmudi.com.ph'


def search(driver, search_parameter, max_pages):
    results = []
    try:
        driver = webdriver.Chrome()

        driver.get(home_page)
        driver.find_element_by_xpath('//*[@id="brandModelInput"]').send_keys(search_parameter)

        try:
            suggestion = WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="brandModelSuggestions"]/ul/li[1]')))
            suggestion.click()
        except Exception as ex:
            print(ex)

        driver.find_element_by_xpath('//*[@id="homepage-search-box"]/div[5]/button').click()
        try:
            while max_pages > 0:
                try:
                    results.extend(extract_content(driver.page_source))
                    next_page = WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="next-page"]/a')))
                    driver.execute_script('arguments[0].click();', next_page)
                except Exception as ex:
                    print(ex)
                max_pages -= 1
        except Exception as ex:
            print(ex)
            print(f'End of the page has been reached.')

    except Exception as ex:
        print(ex)
    finally:
        driver.close()
    return results


def extract_content(html):
    page_soup = soup(html, 'html.parser')
    classified_ads = page_soup.findAll('div', {'class': 'catalog-listing-item-description'})
    page_contents = []
    for classified_ad in classified_ads:
        header = classified_ad.find('h3', {'class': 'item-title'}).find('a')
        price_head = classified_ad.find('h4', {'class': 'item-price'}).find('a')

        title = header.text.strip()
        link = f"{home_page}{header['href']}"
        price = price_head.text.strip()

        print(f"{title} | {link} | {price}")
    
    return page_contents
