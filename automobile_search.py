"""This module searches for popular
automobile online stores in the Philippines.
1.) OLX
2.) carfinder
3.) carmudi
4.) ebay ph
5.) philkotse
"""

from selenium import webdriver
from automobile import olx, carfinder, carmudi, ebayph, philkotse


def output_to_spreadsheet(results):
    with open('auto_search_results.csv', 'w+', encoding='utf-8') as csv:
        csv.write(f"Title,Price,Link\n")
        for item in results:
            print(f"{item.title} | {item.price} | {item.link}")
            csv.write(f"{item.title},{item.price},{item.link}\n")


search_parameter = input('What would you like to search for? ')
max_pages = int(input('Enter the maximum number of pages you would like: '))
print(f'Searching for {search_parameter}...')

driver = webdriver.Chrome()
results = []
try:
    results.extend(olx.search(driver, search_parameter, max_pages))
    results.extend(carfinder.search(driver, search_parameter, max_pages))
    results.extend(ebayph.search(driver, search_parameter, max_pages))
    results.extend(philkotse.search(driver, search_parameter, max_pages))

    # Not working due to click on next page not registering
    # carmudi.search(search_parameter, max_pages)
    output_to_spreadsheet(results)
except Exception as ex:
    print(ex)
finally:
    driver.close()
